#include <stdio.h>
#include <curses.h>
#include <unistd.h>
#include <stdlib.h>

int
main ( void )
{
    WINDOW *ablak;
    ablak = initscr ();

    int x = 0;
    int y = 0;
    int xx = 0;
    int yy = 0;

    int xnov = 1;
    int ynov = 1;

    int mx;
    int my;

    for ( ;; ) {

        getmaxyx ( ablak, my , mx );


        mx = mx*2 ;
        my = my*2 ;

        x = (x - xnov) % mx;
        xx = (xx + xnov) % mx;
        yy = (yy + ynov) % my;
        y = (y - ynov) % my;

        clear();
        
        mvprintw(abs((y + (my - yy)) / 2), abs((x + (mx - xx)) / 2), "o");

        refresh ();

        usleep ( 40000 );
        
    }

    return 0;
}
